﻿using UnityEngine;
using System.Collections;

public class SingletonMonoBehavior<T> : MonoBehaviour where T : SingletonMonoBehavior<T>
{
    private static T m_instance;

    public static T instance
    {
        get
        {
            if (m_instance == null)
            {
                m_instance = GameObject.FindObjectOfType<T>();
                if (m_instance == null)
                {
                    GameObject go = new GameObject(typeof(T).Name);

                    //Debug.Log(typeof(T).Name);
                    m_instance = go.AddComponent<T>();
                    m_instance.transform.SetParent(GameObject.Find("Global").transform);
                }
                if (!m_instance.initialized)
                {
                    m_instance.initialized = true;
                    m_instance.Initialize();
                }
            }

            return m_instance;
        }
    }

    protected bool initialized { get; set; }

    protected virtual void Awake() { }

    protected virtual void Initialize() { }
}
