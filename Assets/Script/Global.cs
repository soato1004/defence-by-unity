﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global : MonoBehaviour
{
    SocketController serverManager;

    void Start()
    {
        serverManager = SocketController.instance;
        serverManager.transform.parent = transform;
    }
}
