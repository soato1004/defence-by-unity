﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameScene : MonoBehaviour
{
    public TMP_Text chat;
    public TMP_InputField input;

    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.Find("Global") == null)
        {
            //GameObject obj = (GameObject)Instantiate(Resources.Load(string.Format("{0}Global", Global.ResourcesPathDef.LOGIN_PATH)));
            //obj.name = "Global";
            //DontDestroyOnLoad(obj);
            GameObject obj = (GameObject)Instantiate(Resources.Load(ResourcePathDef.GLOBAL));
            obj.name = "Global";
            DontDestroyOnLoad(obj);
        }
    }

    public void OnClickSend()
    {
        JSONObject obj = new JSONObject(JSONObject.Type.OBJECT);
        obj.AddField("sMsg", "Hello World!!");
        SocketController.Emit("connection", obj);
        //string str = input.text;
        //input.text = "";
        //chat.text += str + "\n";
    }
}
