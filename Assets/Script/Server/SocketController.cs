﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;

public class SocketController : SingletonMonoBehavior<SocketController>
{
    static private SocketIOComponent socket;

    public void Start()
    {
        GameObject go = GameObject.Find("SocketIO");
        socket = go.GetComponent<SocketIOComponent>();

        socket.On("test", OnTest);
    }

    static public void Emit(string msg, JSONObject obj)
    {
        Debug.Log("============ Emit : " + msg + " DATA : " + obj);
        socket.Emit(msg, obj);
    }

    void OnTest(SocketIOEvent e)
    {
        JSONObject obj = e.data;
    }
}
